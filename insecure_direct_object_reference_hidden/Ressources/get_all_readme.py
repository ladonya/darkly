#!/usr/bin/env python3.6

from urllib import request
from pprint import pprint
from argparse import ArgumentParser
import re

host = 'http://192.168.1.6'
path = '/.hidden/'
pattern = r'<a href=\"([\w/]+)\">[\w/]+</a>'


def get_readme(url, readme):
    contents = str(request.urlopen(url).read())
    res = re.findall(pattern, contents)

    if 'README' in res:
        readme_content = str(request.urlopen(url + 'README').read())
        readme.add(readme_content)
        res.remove('README')

    for path in res:
        new_url = url + path
        get_readme(new_url, readme)


parser = ArgumentParser()
parser.add_argument('--host', type=str, default=None)
args = parser.parse_args()
if args.host:
    host = args.host

readme = set()
get_readme(host + path, readme)
pprint(readme)
